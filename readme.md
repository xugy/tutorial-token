#   TutorialToken

TutorialToken是根据公众号“换币乐”文章《五分钟开发您自己的ERC20代币》所发布的一个ERC20兼容的代币而来。

#  使用步骤

1. 下载代码

```
git clone https://gitee.com/xugy/tutorial-token.git
```

2. 安装依赖

```
sudo npm install -g yarn truffle ganache-cli 
yarn install
```

3. 打开根目录下的.env文件，替换对应网络账户的私钥。MAINNET_PRIVATE_KEY为主网的私钥。 ROPSTEN_PRIVATE_KEY为ropsten测试网络的私钥。内容如下：

```
MAINNET_PRIVATE_KEY="你的主网账户私钥"
ROPSTEN_PRIVATE_KEY="你的ropsten账户私钥“
```
若是开发环境，也可直接 ganache-cli 启动私链

4. 部署合约
```
truffle migrate --reset
```

5. truffle deploy --network ropsten (如果是部署到主网请将参数ropsten换成mainnet)

6. 启动项目
```
npm run dev
```
