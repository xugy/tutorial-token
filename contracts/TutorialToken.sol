pragma solidity ^0.4.23;

import 'zeppelin-solidity/contracts/token/ERC20/MintableToken.sol';

contract TutorialToken is MintableToken {

    // 使用 Ownable.sol 则此处不能重复定义
    // address public owner = 0xf17f52151EbEF6C7334FAD080c5704D77216b732;    // 地址是16进制数字，非字符串
    string public name = "WecashToken";
    string public symbol = "WTH";
    uint8 public decimals = 8;
    uint public INITIAL_SUPPLY = 10 ** 11;  // equal 10 ^ 11, 1000亿

    constructor() public {
        totalSupply_ = INITIAL_SUPPLY * 10 ** uint256(decimals);
        balances[msg.sender] = totalSupply_;
    }

    /**
     * 覆盖 Ownable -> transferOwnership 方法.
     */
    function transferOwnership(address _newOwner) public onlyOwner {
        balances[_newOwner] = balances[owner].add(balances[_newOwner]);  // Owner 余额要转移给 新所有者。
        balances[owner] = 0;    // 清空旧所有人余额。
        super._transferOwnership(_newOwner);
    }
}
